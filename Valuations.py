# -*- coding: utf-8 -*-
"""
Created on Sat Jan 28 13:53:17 2017

@author: Ethan Oppenheim
"""
import urllib2
from cookielib import CookieJar
import datetime
from bs4 import BeautifulSoup
import xlwt

cj = CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))
opener.addheaders = [('User-agnet','Mozilla/5.0')]

def output(data):
    book = xlwt.Workbook()
    sh = book.add_sheet('Valuations')
    col1_name, col2_name, col3_name = 'Program', 'Source', 'Value (cents)'
    sh.write(0, 0, col1_name)
    sh.write(0, 1, col2_name)
    sh.write(0, 2, col3_name)    
    rowNum=0
    for row in data:
        colNum=0
        rowNum+=1
        for value in row:
            sh.write(rowNum, colNum, value)
            colNum+=1        
    book.save('PointValuations.xls')

#Thepointsguy.com
def TPG_Values():
        tpgDict = {}
        postFound, currMonthInt, currYear = False, int(datetime.datetime.now().month)+1, int(datetime.datetime.now().year)+1
        while postFound == False: #Find most recent post from TPG
            try:
                currMonthInt-=1
                currYear-=1
                currMonth = '0'+str(currMonthInt) if len(str(currMonthInt))<2 else str(currMonthInt)
                monthName = datetime.date(1900, int(currMonth), 1).strftime('%B')
                url="http://thepointsguy.com/"+str(currYear)+"/"+currMonth+"/"+monthName.lower()+"-"+str(currYear)+"-monthly-valuations/"
                sourceCode = opener.open(url).read()
                postFound = True
            except BaseException, e:
                print str(e)
                postFound = False
        soup = BeautifulSoup(sourceCode, "lxml")
        #table = soup.find('section', {'class':'article-content'}).find('div', {'class':'article-body-content'}).find('tbody')  
        table = soup.find('tbody')        
        rows = table.findAll('tr')
        for row in rows:
            try:
                column = row.findAll('td') if len(row.findAll('td'))>1 else None
            except: 
                continue
            if column<> None and '(cents)' not in column[3].text: #Dont add header
                partner = column[0].text.encode('utf-8').lower().decode("utf8")
                cents = column[3].text
                tpgDict[partner] = cents
        return tpgDict
    
#Nerdwallet.com
def NW_Values():
    nwDict = {}
    sourceCode = opener.open('https://www.nerdwallet.com/blog/reward-program-reviews/').read()
    soup = BeautifulSoup(sourceCode, "lxml")
    chunkCode = soup.find('div', {'class':'wordpress-content-region'})
    table = chunkCode.findAll('table')[0]
    rows = table.findAll('tr')
    for row in rows:
        column = row.findAll('td')
        if len(column)<2:
            continue
        partner = column[0].text.encode('utf-8').lower()
        cents = column[1].text.encode('utf-8').decode('utf-8').strip().encode('utf-8')
        nwDict[partner] = cents
    return nwDict
        
#Onemileatatime.boardingarea.com
def OM_Values():   
    omDict = {}
    sourceCode = opener.open('http://onemileatatime.boardingarea.com/value-miles-points/').read()
    soup = BeautifulSoup(sourceCode, "lxml")
    chunkCode = soup.find('div', {'class':'entry-content'}).findAll('tbody')
    for table in chunkCode:
        row = [x.findAll('td') for x in table.findAll('tr')]
        for column in row:
            omDict[column[0].text.encode('utf-8').lower()] = column[1].text.encode('utf-8').split(' ')[0]     
    return omDict 
                  
if __name__ == '__main__':
    tpgList = list_key_value = [ [k,'TPG',v] for k, v in TPG_Values().items() ]
    nwList = list_key_value = [ [k,'Nerd Wallet',v] for k, v in NW_Values().items() ]
    omList = list_key_value = [ [k,'One Mile',v] for k, v in OM_Values().items() ]
    finalList = tpgList+nwList+omList
    output(finalList)