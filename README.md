# README #

### What is this repository for? ###

Valuations.(exe/py) pulls most recent point valuations from multiple rewards blogs and outputs into excel. Used to refresh data for tableau dashboard.

### On Windows: ###
Run Valuations.exe

### On Linux/OSX (via cmd/terminal) ###
* pip install bs4
* pip install xlwt
* Place python script in same directory as excel data file and "python Valuations.py"
* Ensure data file is named "PointValuations.xls"

### Tips ###
* The script will REPLACE the excel file with the most updated values it can find
* If using an extract in tableau, be sure to refresh the extract after running the script
* Feel free to add your own custom valuations! Make sure you populate column B with the name of your valuation (i.e. 'My Value')